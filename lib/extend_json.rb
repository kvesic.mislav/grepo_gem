#scr: https://stackoverflow.com/questions/18808031/parsing-a-json-string-with-duplicate-keys-in-ruby
class DuplicateKeyChecker
  attr :keys
  @@double_keys = []
  def []=(key, _value)
    @keys ||= []
    if @keys.include?(key)
      @@double_keys << {key: key, value: _value}
    else
      @keys << key
    end
  end

  def get_duplicates
    return @@double_keys
  end
end
