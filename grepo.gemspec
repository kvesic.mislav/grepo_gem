Gem::Specification.new do |s|
  s.name        = 'grepo'
  s.version     = '0.1.7'
  s.date        = '2019-02-07'
  s.summary     = "Grepo will grep for yaml and json keys"
  s.description = "Grepo is a small gem that helps you to clean up your yaml and json files. It's usefull if you are having your translations in yaml or json files especially if the files are numerous and have a lot of lines. It can detect keys you aren’t using, can find duplicated keys and compere two files to find the missing keys. It won’t remove the targeted keys neither it will alter your files in any way, just grep for possible problems."
  s.authors     = ["Mislav Kvesić"]
  s.email       = 'kvesic.mislav@gmail.com'
  s.require_paths = ["lib"]
  s.files       = ["lib/grepo.rb", "lib/extend_json.rb"]
  s.homepage    =
    'http://rubygems.org/gems/grepo'
  s.license       = 'MIT'
  s.executables << 'grepo'
end

